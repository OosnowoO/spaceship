using System;
using Enemy;
using Manager;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        [SerializeField] private double fireRate = 1f;

        [SerializeField] private AudioClip enemyDeadSound;
        [SerializeField] private float enemyDeadVolume = 0.5f;
        
        [SerializeField] private AudioClip enemyFireSound;
        [SerializeField] private float enemyFireVolume = 0.3f;
        
        private float fireCounter = 0f;
        
        [SerializeField] private float maxSpawnRateInSeconds;
        public GameObject EnemyShip;
        
        
        private void Start()
        {
            Invoke("illusionSpawn", maxSpawnRateInSeconds);
            
            InvokeRepeating("IncreaseSpawnRate", 0f ,30f);
        }
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            AudioSource.PlayClipAtPoint(enemyDeadSound, Camera.main.transform.position,enemyDeadVolume);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        void illusionSpawn()
        {
            Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

            Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, (float) 0.82));
            
            GameObject anEnemy = Instantiate(EnemyShip);
            anEnemy.transform.position = new  Vector2(Random.Range(min.x , max.x), max.y);
            ScheduleNextEnemySpawn();
        }
        void ScheduleNextEnemySpawn()
        {
            float spawnInseconds;
            if (maxSpawnRateInSeconds > 1f)
            {
                Random.Range(1f, maxSpawnRateInSeconds);
            }
            else
            {
                spawnInseconds = 1f;
                Invoke("SpawnEnemy", spawnInseconds);
            }
            
        }
        void IncreaseSpawnRate()
        {
            if (maxSpawnRateInSeconds > 1f)
                maxSpawnRateInSeconds--;
            if (maxSpawnRateInSeconds == 1f)
                CancelInvoke("IncreaseSpawnRate");
        }
        public override void Fire()
        {
            
            fireCounter += Time.deltaTime;

            if (fireCounter >= fireRate)
            {
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                
                bullet.Init(Vector2.down);
                fireCounter = 0f;
                AudioSource.PlayClipAtPoint(enemyFireSound, Camera.main.transform.position,enemyFireVolume);
            }
            
        }
    }
}