using System;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        
        //Dead Sound
        [SerializeField] private AudioClip playerDeadSound;
        [SerializeField] private float playerDeadVolume = 0.5f;
        
        //Fire Sound
        [SerializeField] private AudioClip playerFireSound;
        [SerializeField] private float playerFireVolume = 0.3f;
        [SerializeField] private AudioClip playerMissileSound;
        [SerializeField] private float playerMissileVolume = 0.3f;
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            Debug.Assert(missilePosition1 != null, "missilePosition cannot be null");
            Debug.Assert(missilePosition2 != null, "missilePosition cannot be null");
            Debug.Assert(playerFireSound != null, "playerFireSound cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
            base.Init(hp, speed, missileBullet);
        }

        public override void Fire()
        {
            AudioSource.PlayClipAtPoint(playerFireSound, Camera.main.transform.position,playerFireVolume);
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);

            if (Hp <= 100)
            {
                AudioSource.PlayClipAtPoint(playerMissileSound, Camera.main.transform.position,playerMissileVolume);
                bullet = Instantiate(missileBullet, missilePosition1.position, Quaternion.identity);
                bullet.Init(Vector2.up);
                bullet = Instantiate(missileBullet, missilePosition2.position, Quaternion.identity);
                bullet.Init(Vector2.up);
            }
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            AudioSource.PlayClipAtPoint(playerDeadSound, Camera.main.transform.position,playerDeadVolume);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}