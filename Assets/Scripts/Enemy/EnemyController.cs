﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spaceship;
using Unity.Jobs.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] EnemySpaceship enemySpaceship;
        [SerializeField] private float speed = 0f;

        

        private void Update()
        {
            Vector2 position = transform.position;
            new Vector2(position.x , position.y - speed * Time.deltaTime);

            Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
            if (transform.position.y < min.y)
            {
                Destroy(gameObject);
            }
            enemySpaceship.Fire();
        }
        
    }    
}

